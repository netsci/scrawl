"""
Utility functions.
"""


def compose(*functions):
    """
    Compose the given functions to one function.

    The functions are composed such that the last function is applied on the input,
    then the second last function is applied on the result of the last function, and so
    on.

    Parameters
    ----------
    *functions : callable
        The functions to compose.

    Returns
    -------
    callable
        The composed function.

    Examples
    --------
    >>> x = lambda a: (a, 2)
    >>> y = lambda a: (a, 1)
    >>> z = compose(x, y)
    >>> z(0)
    ((0, 1), 2)
    """

    def inner(arg):  # numpydoc ignore=GL08
        for f in reversed(functions):
            arg = f(arg)
        return arg

    return inner
