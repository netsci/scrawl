Learning From Simplicial Data Based on Random Walks and 1D Convolutions
=======================================================================

[[OpenReview]](https://openreview.net/forum?id=OsGUnYOzii)

```bibtex
@inproceedings{Frantzen:2023,
    title={{Learning From Simplicial Data Based on Random Walks and 1D Convolutions}},
    author={Florian Frantzen and Michael T. Schaub},
    booktitle={The Twelfth International Conference on Learning Representations},
    year={2024},
    url={https://openreview.net/forum?id=OsGUnYOzii}
}
```

Requirements
------------

- Python 3.10 or higher
- For package requirements, see [`pyproject.toml`](pyproject.toml)

Use the provided installation script `install.sh` to install all requirements.
If desired, change the `CUDA` variable at the top of the script according to your system.

**Note**: [TopoNetX](https://github.com/pyt-team/TopoNetX) and [TopoModelX](https://github.com/pyt-team/TopoModelX) are installed from source as there are no public releases yet.
