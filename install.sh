#! /bin/bash
CUDA=cpu

pip3 install torch==2.1.* --index-url https://download.pytorch.org/whl/cpu
pip3 install torch_geometric torch_scatter -f https://data.pyg.org/whl/torch-2.1.0+${CUDA}.html

pip3 install -e '.[dev]'
